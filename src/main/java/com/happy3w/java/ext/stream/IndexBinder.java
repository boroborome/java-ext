package com.happy3w.java.ext.stream;

public class IndexBinder {
    private int index = 0;
    public static class IndexItem<T> {
        private final int index;
        private final T value;

        public IndexItem(int index, T value) {
            this.index = index;
            this.value = value;
        }

        public int getIndex() {
            return index;
        }

        public T getValue() {
            return value;
        }
    }

    public <T> IndexItem<T> zip(T item) {
        return new IndexItem<>(index++, item);
    }
}
