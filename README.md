# java-ext

本项目是一个工具箱项目，提供一组不依赖于任何框架的工具

- FileUtils
  - enumerateFiles方法，可以枚举提供目录中所有文件
  - enumerateFilesByPattern方法用于按照模式枚举所有文件，支持如下几种模板
  
| Pattern              | 含义                                |
|----------------------|-----------------------------------|
| /abc/*/config.txt    | 枚举/abc/下面所有紧邻目录中的config.txt文件     |
| /abc/**/config.txt   | 枚举/abc/下面任意层目录中的config.txt文件      |
| /abc/**/config-*.txt | 枚举/abc/下面任意层目录中的"config-"开头的txt文件 |
| /abc/def/ | 没有通配符，只返回这个文件或者文件夹        |
