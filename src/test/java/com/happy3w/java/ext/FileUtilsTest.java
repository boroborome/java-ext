package com.happy3w.java.ext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

;

public class FileUtilsTest {

    @Test
    public void should_enum_normal_file() {
        List<String> files = FileUtils.enumerateFilesByPattern("./src/test/resources/files/config.txt")
                .map(file -> file.getAbsolutePath())
                .collect(Collectors.toList());

        shouldMatch(Arrays.asList("/src/test/resources/files/config.txt"), files);
    }

    @Test
    public void should_enum_single_dir_files() {
        List<String> files = FileUtils.enumerateFilesByPattern("./src/test/resources/files/")
                .map(file -> file.getAbsolutePath())
                .collect(Collectors.toList());

        shouldMatch(Arrays.asList("/src/test/resources/files"),
                files);
    }

    @Test
    public void should_enum_single_dir_normal_file() {
        List<String> files = FileUtils.enumerateFilesByPattern("./src/test/resources/files/*/config.txt")
                .map(file -> file.getAbsolutePath())
                .collect(Collectors.toList());

        shouldMatch(Arrays.asList("/src/test/resources/files/a/config.txt",
                        "/src/test/resources/files/b/config.txt"),
                files);
    }

    @Test
    public void should_enum_deep_dir_files() {
        List<String> files = FileUtils.enumerateFilesByPattern("src/test/resources/files/**/config.*")
                .map(file -> file.getAbsolutePath())
                .collect(Collectors.toList());

        shouldMatch(Arrays.asList("/src/test/resources/files/a/config.txt",
                        "/src/test/resources/files/a/c/config.txt",
                        "/src/test/resources/files/b/config.txt",
                        "/src/test/resources/files/config.txt"),
                files);
    }

    private void shouldMatch(List<String> expectFiles, List<String> files) {
        List<String> filesShouldMatch = new ArrayList<>(expectFiles);
        List<String> filesLost = new ArrayList<>();

        for (String file : files) {
            String matchFile = findMatchFile(file, filesShouldMatch);
            if (matchFile == null) {
                filesLost.add(file);
            } else {
                filesShouldMatch.remove(matchFile);
            }
        }

        if (!filesShouldMatch.isEmpty() || !filesLost.isEmpty()) {
            Assertions.fail(MessageFormat.format("Files expected but not found:{0}\nFiles found but not expected:{1}", filesShouldMatch, filesLost));
        }
    }

    private String findMatchFile(String file, List<String> filesShouldMatch) {
        for (String fileShouldMatch : filesShouldMatch) {
            if (file.endsWith(fileShouldMatch)) {
                return fileShouldMatch;
            }
        }
        return null;
    }
}
