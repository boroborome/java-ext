package com.happy3w.java.ext;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FileUtils {
    private FileUtils() {

    }

    public static Stream<File> enumerateFilesByPattern(String pattern) {
        int wildcardIndex = pattern.indexOf('*');
        if (wildcardIndex < 0) {
            return Stream.of(new File(pattern));
        }

        int slashIndex = pattern.lastIndexOf('/', wildcardIndex);
        String parentDir = slashIndex >= 0 ? pattern.substring(0, slashIndex) : ".";
        String leftPatten = pattern.substring(slashIndex + 1);

        Stream<File> fileStream = Stream.of(new File(parentDir));
        for (String patternItem : leftPatten.split("/")) {
            fileStream = fileStream.flatMap(file -> enumFileBySinglePattern(file, patternItem));
        }

        return fileStream;
    }

    private static Stream<File> enumFileBySinglePattern(File file, String patternItem) {
        if (patternItem.equals("**")) {
            return StreamSupport.stream(new EnumerateFileSpliterator(file, false, true), false);
        } else {
            Pattern fileMatcher = Pattern.compile("^" + patternItem.replaceAll("\\*", "[^/]*") + "$");
            File[] matchedFiles = file.listFiles((dir, name) -> fileMatcher.matcher(name).matches());
            return (matchedFiles == null || matchedFiles.length == 0)
                    ? Stream.empty()
                    : Arrays.asList(matchedFiles).stream();
        }
    }

    public static Stream<File> enumerateFiles(File file) {
        if (file.isFile()) {
            return Stream.of(file);
        }

        return StreamSupport.stream(new EnumerateFileSpliterator(file), false);
    }

    public static Stream<String> lines(InputStream input) {
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        try {
            return br.lines().onClose(() -> {
                try {
                    br.close();
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });
        } catch (Error|RuntimeException e) {
            try {
                br.close();
            } catch (IOException ex) {
                try {
                    e.addSuppressed(ex);
                } catch (Throwable ignore) {}
            }
            throw e;
        }
    }
}
