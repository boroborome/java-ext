package com.happy3w.java.ext.stream;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountHolder<T> {
    private T key;
    private long count;

    public CountHolder(T key) {
        this.key = key;
    }

    public void increase(long value) {
        count += value;
    }

    @Override
    public String toString() {
        return key + "=" + count;
    }
}
