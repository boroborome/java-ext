package com.happy3w.java.ext.stream;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

public class ParallelSpliterator<T> extends Spliterators.AbstractSpliterator<T> {
    private final Iterator<T> innerIt;

    public ParallelSpliterator(Iterator<T> innerIt, int additionalCharacteristics) {
        super(Long.MAX_VALUE, additionalCharacteristics);
        this.innerIt = innerIt;
    }

    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        if (innerIt.hasNext()) {
            action.accept(innerIt.next());
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<T> trySplit() {
        if (innerIt.hasNext()) {
            return Spliterators.spliterator(new Object[]{innerIt.next()}, Spliterator.SIZED);
        }

        return null;
    }
}