package com.happy3w.java.ext.stream;

import com.happy3w.java.ext.stream.IndexBinder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

class IndexBinderTest {

    @Test
    void should_zip_success() {
        String result = Stream.of("A", "B", "C")
                .map(new IndexBinder()::zip)
                .map(ib -> ib.getValue() + ib.getIndex())
                .collect(Collectors.joining(","));
        Assertions.assertEquals("A0,B1,C2", result);
    }
}
