package com.happy3w.java.ext;

public class BooleanUtils {
    private BooleanUtils() {

    }

    public static boolean valueOf(String strValue) {
        return "yes".equalsIgnoreCase(strValue)
                || "true".equalsIgnoreCase(strValue);
    }
}
