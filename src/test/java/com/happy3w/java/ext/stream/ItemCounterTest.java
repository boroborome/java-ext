package com.happy3w.java.ext.stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ItemCounterTest {

    @Test
    void should_count_success() {
        List<CountHolder<String>> result = Stream.of("a", "b", "c", "a", "b", "a")
                .collect(ItemCounter.counter())
                .sorted(Comparator.comparing(CountHolder::getKey))
                .collect(Collectors.toList());
        Assertions.assertEquals("[a=3, b=2, c=1]", result.toString());
    }
}
