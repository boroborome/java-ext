package com.happy3w.java.ext;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Spliterators;
import java.util.function.Consumer;

public class EnumerateFileSpliterator extends Spliterators.AbstractSpliterator<File> {
    private Deque<File> fileStack = new ArrayDeque<>();
    private boolean acceptFile;
    private boolean acceptDir;

    public EnumerateFileSpliterator(File file) {
        this(file, true, false);
    }

    public EnumerateFileSpliterator(File file, boolean acceptFile, boolean acceptDir) {
        super(0, 0);
        fileStack.push(file);
        this.acceptFile = acceptFile;
        this.acceptDir = acceptDir;
    }


    @Override
    public boolean tryAdvance(Consumer<? super File> action) {
        while (!fileStack.isEmpty()) {
            File file = fileStack.pop();
            if (acceptFile && file.isFile()) {
                action.accept(file);
                return true;
            }
            if (file.isDirectory()) {
                for (File f : file.listFiles()) {
                    fileStack.push(f);
                }
                if (acceptDir) {
                    action.accept(file);
                    return true;
                }
            }
        }
        return false;
    }
}
